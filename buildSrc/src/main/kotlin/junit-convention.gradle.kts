plugins {
    java apply false
}

tasks.withType<Test> {
    useJUnitPlatform()
}

dependencies {
    testImplementation(Libs.Tests.junit)
    testImplementation(Libs.Tests.assertJ)
}
