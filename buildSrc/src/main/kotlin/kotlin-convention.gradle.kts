import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("java-convention")
    kotlin("jvm")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(platform("org.jetbrains.kotlin:kotlin-bom:1.4.20"))
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "15"
        javaParameters = true
        freeCompilerArgs = listOf(
            "-Xinline-classes",
            "-Xno-param-assertions",
            "-Xno-call-assertions",
            "-Xno-receiver-assertions",
            "-Xopt-in=kotlin.RequiresOptIn"
        )
    }
}
