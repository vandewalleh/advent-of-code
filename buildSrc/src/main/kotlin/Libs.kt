@file:Suppress("SpellCheckingInspection")

object Libs {

    object Arrow {
        const val version = "0.11.0"
        const val core = "io.arrow-kt:arrow-core:$version"
        const val optics = "io.arrow-kt:arrow-optics:$version"
        const val fx = "io.arrow-kt:arrow-fx:$version"
    }

    const val eclipseCollections = "org.eclipse.collections:eclipse-collections:10.4.0"

    object Jmh {
        const val core = "org.openjdk.jmh:jmh-core:1.26"
        const val processor = "org.openjdk.jmh:jmh-generator-annprocess:1.26"
    }

    object Micronaut {
        private const val version = "2.2.0"
        const val inject = "io.micronaut:micronaut-inject:$version"
        const val core = "io.micronaut:micronaut-core:$version"
        const val kotlin = "io.micronaut.kotlin:micronaut-kotlin-extension-functions:$version"
        const val processor = "io.micronaut:micronaut-inject-java:$version"
    }

    object Slf4J {
        const val api = "org.slf4j:slf4j-api:1.7.25"
        const val simple = "org.slf4j:slf4j-simple:1.7.30"
        const val kotlin = "io.github.microutils:kotlin-logging-jvm:2.0.3"
    }

    object Tests {
        const val assertJ = "org.assertj:assertj-core:3.18.1"
        const val junit = "org.junit.jupiter:junit-jupiter:5.7.0"
        const val jimfs = "com.google.jimfs:jimfs:1.1"
    }

}
