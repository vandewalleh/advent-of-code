plugins {
    id("java-convention")
}

dependencies {
    implementation(project(":utils"))

    annotationProcessor(Libs.Micronaut.processor)

    implementation(Libs.eclipseCollections)

    testImplementation(Libs.Jmh.core)
    testAnnotationProcessor(Libs.Jmh.processor)
}
