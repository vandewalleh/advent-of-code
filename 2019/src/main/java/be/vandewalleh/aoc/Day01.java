package be.vandewalleh.aoc;

import be.vandewalleh.aoc.utils.factory.Days;
import be.vandewalleh.aoc.utils.input.Day;
import be.vandewalleh.aoc.utils.input.Input;
import be.vandewalleh.aoc.utils.input.Lines;

@Day(1)
public class Day01 {

    public static void main(String[] args) {
        var day = Days.createDay(Day01.class);
        System.out.println(day.part1());
        System.out.println(day.part2());
    }

    private final int[] input;

    public Day01(@Lines Input<int[]> input) {
        this.input = input.getValue();
    }

    private int fuel(int mass) {
        return (mass / 3) - 2;
    }

    public int part1() {
        var total = 0;
        for (var mass : input) {
            total += fuel(mass);
        }
        return total;
    }

    public int part2() {
        var total = 0;
        for (var mass : input) {
            int fuel = fuel(mass);
            total += fuel;
            while (true) {
                fuel = fuel(fuel);
                if (fuel <= 0) break;
                total += fuel;
            }
        }
        return total;
    }
}
