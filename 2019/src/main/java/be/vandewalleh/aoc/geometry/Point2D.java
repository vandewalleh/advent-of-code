package be.vandewalleh.aoc.geometry;

public record Point2D(int x, int y) {
    public static Point2D origin = new Point2D(0, 0);

    public int manhattanDistance() {
        return Math.abs(x) + Math.abs(y);
    }

    public Point2D translate(Point2D other) {
        return new Point2D(this.x + other.x, this.y + other.y);
    }
}
