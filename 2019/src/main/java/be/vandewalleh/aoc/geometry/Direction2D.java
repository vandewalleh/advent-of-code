package be.vandewalleh.aoc.geometry;

public enum Direction2D {
    Up(0, -1), Down(0, 1), Right(1, 0), Left(-1, 0);

    public final Point2D point;

    Direction2D(int x, int y) {
        this.point = new Point2D(x, y);
    }

    public static Direction2D from(char direction) {
        switch (direction) {
            case 'U' -> {
                return Direction2D.Up;
            }
            case 'D' -> {
                return Direction2D.Down;
            }
            case 'R' -> {
                return Direction2D.Right;
            }
            case 'L' -> {
                return Direction2D.Left;
            }
            default -> throw new IllegalArgumentException("Unexpected value: " + direction);
        }
    }
}
