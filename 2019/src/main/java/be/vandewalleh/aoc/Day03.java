package be.vandewalleh.aoc;

import be.vandewalleh.aoc.geometry.Direction2D;
import be.vandewalleh.aoc.geometry.Point2D;
import be.vandewalleh.aoc.utils.factory.Days;
import be.vandewalleh.aoc.utils.input.Day;
import be.vandewalleh.aoc.utils.input.Input;
import be.vandewalleh.aoc.utils.input.Lines;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Day(3)
public class Day03 {

    public static void main(String[] args) {
        var day = Days.createDay(Day03.class);
        System.out.println(day.part1());
        System.out.println(day.part2());
    }

    private final String[] wireA;
    private final String[] wireB;

    public Day03(@Lines Input<List<String>> input) {
        this.wireA = input.getValue().get(0).split(",");
        this.wireB = input.getValue().get(1).split(",");
    }

    private List<Point2D> path(String[] wire) {
        var points = new ArrayList<Point2D>();
        var point = Point2D.origin;
        for (var section : wire) {
            var direction = Direction2D.from(section.charAt(0));
            var count = Integer.parseInt(section.substring(1));
            for (int i = 0; i < count; i++) {
                point = point.translate(direction.point);
                points.add(point);
            }
        }
        return points;
    }

    private int part1() {
        var pathA = path(wireA);
        var pathB = path(wireB);

        var intersections = new HashSet<>(pathA);
        intersections.retainAll(pathB);

        return intersections.stream().mapToInt(Point2D::manhattanDistance).min().orElse(-1);
    }

    private int part2() {
        var pathA = path(wireA);
        var pathB = path(wireB);

        var intersections = new HashSet<>(pathA);
        intersections.retainAll(pathB);

        return intersections.stream().mapToInt(p -> pathA.indexOf(p) + pathB.indexOf(p) + 2).min().orElse(-1);
    }
}
