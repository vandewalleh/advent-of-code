package be.vandewalleh.aoc;

import be.vandewalleh.aoc.intcode.IntCodeInterpreter;
import be.vandewalleh.aoc.utils.factory.Days;
import be.vandewalleh.aoc.utils.input.Csv;
import be.vandewalleh.aoc.utils.input.Day;
import be.vandewalleh.aoc.utils.input.Input;

@Day(2)
public class Day02 {

    public static void main(String[] args) {
        var day = Days.createDay(Day02.class);
        System.out.println(day.part1());
        System.out.println(day.part2());
    }

    private final int[] input;

    public Day02(@Csv Input<int[]> input) {
        this.input = input.getValue();
    }

    private long runInterpreterWith(int noun, int verb) {
        var interpreter = new IntCodeInterpreter(input);
        interpreter.setNoun(noun);
        interpreter.setVerb(verb);
        interpreter.run();
        return interpreter.getOutput();
    }

    public long part1() {
        return runInterpreterWith(12, 2);
    }

    public int part2() {
        for (var noun = 0; noun < 99; noun++) {
            for (var verb = 0; verb < 99; verb++) {
                if (runInterpreterWith(noun, verb) == 19690720L) return 100 * noun + verb;
            }
        }
        return -1;
    }
}
