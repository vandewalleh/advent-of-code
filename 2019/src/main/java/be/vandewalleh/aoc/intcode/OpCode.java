package be.vandewalleh.aoc.intcode;

import java.util.Optional;

public enum OpCode {
    Add(1, 3),
    Multiply(2, 3),
    Halt(99, 0),
    In(3, 1),
    Out(4, 1),
    JumpIfTrue(5, 2),
    JumpIfFalse(6, 2),
    LessThan(7, 3),
    Equals(8, 3),
    ;

    public final int value;
    public final int params;

    OpCode(int value, int params) {
        this.value = value;
        this.params = params;
    }

    public static OpCode from(int value) {
        for (OpCode opCode : OpCode.values()) {
            if (opCode.value == value) return opCode;
        }
        throw new IllegalArgumentException("Unsupported OpCode " + value);
    }
}
