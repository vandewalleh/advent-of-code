package be.vandewalleh.aoc.intcode;

public enum Mode {
    Positional, Immediate;

    public static Mode of(char representation) {
        if (representation == '0') return Mode.Positional;
        else if (representation == '1') return Mode.Immediate;
        else throw new IllegalArgumentException("Unsupported Mode " + representation);
    }
}
