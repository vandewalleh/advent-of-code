package be.vandewalleh.aoc.intcode;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

public class IntCodeInterpreter {
    private final Deque<Long> outputs = new ArrayDeque<>();
    private final long[] memory;
    private long input;

    public IntCodeInterpreter(int[] memory) {
        this.memory = Arrays.stream(memory).asLongStream().toArray();
    }

    public void setNoun(long noun) {
        memory[1] = noun;
    }

    public void setVerb(long verb) {
        memory[2] = verb;
    }

    public void setInput(long input) {
        this.input = input;
    }

    public long getOutput() {
        return memory[0];
    }

    // private ?
    public Deque<Long> getOutputs() {
        return outputs;
    }

    private long readArgument(Mode mode, long param) {
        switch (mode) {
            case Positional -> {
                return memory[(int) param];
            }
            case Immediate -> {
                return param;
            }
            default -> throw new IllegalArgumentException();
        }
    }

    private void writeArgument(long param, long value) {
        memory[(int) param] = value;
    }

    private Mode[] modes(long instruction) {
        var str = String.format("%5d", instruction).replace(' ', '0');
        return new Mode[]{
            Mode.of(str.charAt(2)),
            Mode.of(str.charAt(1)),
            Mode.of(str.charAt(0))
        };
    }

    public void run() {
        var pointer = 0;

        loop:
        while (true) {
            var jumped = false;
            var instruction = memory[pointer];
            var opCode = OpCode.from(((int) instruction) % 100);
            var args = Arrays.copyOfRange(memory, pointer + 1, pointer + opCode.params + 1);
            var modes = modes(instruction);
            switch (opCode) {
                case Add -> {
                    var value = readArgument(modes[0], args[0]) + readArgument(modes[1], args[1]);
                    writeArgument(args[2], value);
                }
                case Multiply -> {
                    var value = readArgument(modes[0], args[0]) * readArgument(modes[1], args[1]);
                    writeArgument(args[2], value);
                }
                case In -> {
                    writeArgument(args[0], input);
                }
                case Out -> {
                    outputs.push(readArgument(modes[0], args[0]));
                }
                case JumpIfTrue -> {
                    if (readArgument(modes[0], args[0]) != 0) {
                        pointer = (int) readArgument(modes[1], args[1]);
                        jumped = true;
                    }
                }
                case JumpIfFalse -> {
                    if (readArgument(modes[0], args[0]) == 0) {
                        pointer = (int) readArgument(modes[1], args[1]);
                        jumped = true;
                    }
                }
                case LessThan -> {
                    var value = readArgument(modes[0], args[0]) < readArgument(modes[1], args[1]) ? 1 : 0;
                    writeArgument(args[2], value);
                }
                case Equals -> {
                    var value = readArgument(modes[0], args[0]) == readArgument(modes[1], args[1]) ? 1 : 0;
                    writeArgument(args[2], value);
                }
                case Halt -> {
                    break loop;
                }

            }
            if (!jumped) pointer += opCode.params + 1;
        }
    }
}
