package be.vandewalleh.aoc;

import be.vandewalleh.aoc.intcode.IntCodeInterpreter;
import be.vandewalleh.aoc.utils.factory.Days;
import be.vandewalleh.aoc.utils.input.Csv;
import be.vandewalleh.aoc.utils.input.Day;
import be.vandewalleh.aoc.utils.input.Input;

@Day(5)
public class Day05 {

    public static void main(String[] args) {
        var day = Days.createDay(Day05.class);
        System.out.println(day.part1());
        System.out.println(day.part2());
    }

    private final int[] input;

    public Day05(@Csv Input<int[]> input) {
        this.input = input.getValue();
    }

    private Long part1() {
        var interpreter = new IntCodeInterpreter(input);
        interpreter.setInput(1);
        interpreter.run();
        return interpreter.getOutputs().peek();
    }

    private Long part2() {
        var interpreter = new IntCodeInterpreter(input);
        interpreter.setInput(5);
        interpreter.run();
        return interpreter.getOutputs().peek();
    }
}
