package be.vandewalleh.aoc;

import be.vandewalleh.aoc.utils.factory.Days;
import be.vandewalleh.aoc.utils.input.Day;
import be.vandewalleh.aoc.utils.input.Input;
import be.vandewalleh.aoc.utils.input.Text;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Day(4)
public class Day04 {

    public static void main(String[] args) {
        var day = Days.createDay(Day04.class);
        System.out.println(day.part1());
        System.out.println(day.part2());
    }

    private final int min;
    private final int max;

    public Day04(@Text Input<String> input) {
        var spl = input.getValue().split("-", 2);
        min = Integer.parseInt(spl[0]);
        max = Integer.parseInt(spl[1]);
    }

    private boolean isNotDecreasing(int[] ints) {
        for (int i = 0; i < 5; i++) {
            int a = ints[i], b = ints[i + 1];
            if (b < a) return false;
        }
        return true;
    }

    private boolean hasPair(int[] ints) {
        for (int i = 0; i < 5; i++) {
            int a = ints[i], b = ints[i + 1];
            if (a == b) return true;
        }
        return false;
    }

    private boolean hasGroup(int[] ints) {
        var occurrences = new int[10];
        for (int i = 0; i < 5; i++) {
            int a = ints[i], b = ints[i + 1];
            if (a == b) occurrences[a]++;
        }
        return Arrays.stream(occurrences).anyMatch(e -> e == 1);
    }

    private Stream<int[]> intStream() {
        return IntStream.rangeClosed(min, max)
            .mapToObj(String::valueOf)
            .map(String::toCharArray)
            .map(e -> {
                var ints = new int[6];
                for (int i = 0; i < 6; i++) {
                    ints[i] = Integer.parseInt(String.valueOf(e[i]));
                }
                return ints;
            });
    }

    private long part1() {
        return intStream()
            .filter(this::isNotDecreasing)
            .filter(this::hasPair)
            .count();
    }

    private long part2() {
        return intStream()
            .filter(this::isNotDecreasing)
            .filter(this::hasGroup)
            .count();
    }
}
