plugins {
    id("advent-of-code-downloader")
}

adventOfCode {
    year = 2020
    session = file(".session").readText().trim()
}
