package be.vandewalleh.aoc.utils.input

import io.micronaut.core.annotation.AnnotationMetadataProvider
import io.micronaut.core.annotation.AnnotationValue
import java.util.*

internal inline fun <reified T : Annotation> AnnotationMetadataProvider.findAnnotation(): Optional<AnnotationValue<T>> =
    findAnnotation(T::class.java)

internal inline fun <reified T : Annotation> AnnotationMetadataProvider.hasAnnotation(): Boolean =
    findAnnotation(T::class.java).isPresent
