package be.vandewalleh.aoc.utils.input

/**
 * Wrapper class so that micronaut is not confused with an other injectable
 * container type when using an iterable / array
 */
data class Input<T>(val value: T)
