@file:Suppress("unused")

package be.vandewalleh.aoc.utils.input

import io.micronaut.context.annotation.Prototype
import io.micronaut.core.annotation.Introspected
import javax.inject.Qualifier

@Prototype
@Qualifier
@Introspected
@Target(AnnotationTarget.CLASS)
annotation class Day(val value: Int)

@Qualifier
@Prototype
annotation class DayInput

@DayInput
annotation class Csv

@DayInput
annotation class Text

@DayInput
annotation class Lines

@DayInput
annotation class Groups
