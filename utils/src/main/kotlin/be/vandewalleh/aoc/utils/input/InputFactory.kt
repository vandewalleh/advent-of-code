package be.vandewalleh.aoc.utils.input

import io.micronaut.context.annotation.Factory
import io.micronaut.inject.InjectionPoint
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.readLines
import kotlin.io.path.readText

/**
 * Load challenge inputs and convert them to the appropriate format
 *
 * contains horrible hacks while waiting for generic support in bean injection
 * @see [micronaut-2775](https://github.com/micronaut-projects/micronaut-core/issues/2775)
 */
@Factory
@ExperimentalPathApi
class InputFactory(private val resourceLoader: ResourceLoader) {

    @Csv
    fun csv(injectionPoint: InjectionPoint<*>): Input<*> =
        when (val param = injectionPoint.typeNameOfAnnotation<Csv>()) {
            INT_ARRAY -> injectionPoint
                .read()
                .split(",")
                .map { it.toInt() }
                .toIntArray()
                .wrap()
            LONG_ARRAY -> injectionPoint
                .read()
                .split(",")
                .map { it.toLong() }
                .toLongArray()
                .wrap()
            else -> error("Unsupported type $param")
        }

    @Lines
    fun lines(injectionPoint: InjectionPoint<*>): Input<*> =
        when (val param = injectionPoint.typeNameOfAnnotation<Lines>()) {
            INT_ARRAY -> injectionPoint
                .lines()
                .map { it.toInt() }
                .toList()
                .toIntArray()
                .wrap()
            LONG_ARRAY -> injectionPoint
                .lines()
                .map { it.toLong() }
                .toList()
                .toLongArray()
                .wrap()
            STRING_LIST -> injectionPoint
                .lines()
                .toList()
                .wrap()
            else -> error("Unsupported type $param")
        }

    @Text
    fun text(injectionPoint: InjectionPoint<*>): Input<String> =
        injectionPoint.read().wrap()

    @Groups
    fun groups(injectionPoint: InjectionPoint<*>): Input<List<List<String>>> =
        injectionPoint.read().split("\n\n").map { it.lines() }.wrap()


    private fun <T> T.wrap() = Input(this)

    private inline fun <reified T : Annotation> InjectionPoint<*>.typeNameOfAnnotation() = declaringBean
        .constructor
        .arguments
        .find { it.hasAnnotation<T>() }
        ?.typeName
        ?.removePrefix("be.vandewalleh.aoc.utils.input.Input<")
        ?.removeSuffix(">")
        ?: error("??")

    private fun InjectionPoint<*>.path() = resourceLoader.ofDay(getDay(this))
    private fun InjectionPoint<*>.read() = path().readText().trim()
    private fun InjectionPoint<*>.lines() = path().readLines()
        .asSequence()
        .filter { it.isNotBlank() }

    private fun getDay(injectionPoint: InjectionPoint<*>): Int {
        val dayAnnotation = injectionPoint
            .declaringBean
            .findAnnotation<Day>()

        if (dayAnnotation.isEmpty)
            error("@DayInput cannot only be used on classes annotated with ${Day::class.qualifiedName}")

        return dayAnnotation.get().intValue("value").asInt
    }

    companion object {
        private const val INT_ARRAY = "int[]"
        private const val LONG_ARRAY = "long[]"
        private const val STRING_LIST = "java.util.List<java.lang.String>"
    }

}
