package be.vandewalleh.aoc.utils.input

import java.io.File
import java.nio.file.Path
import javax.inject.Singleton

interface ResourceLoader {
    fun ofDay(day: Int): Path
}

@Singleton
class ResourceLoaderImpl : ResourceLoader {
    override fun ofDay(day: Int): Path {
        val resourcePath = buildString {
            append("/day")
            append(day.toString().padStart(2, '0'))
            append(".txt")
        }
        val url = javaClass.getResource(resourcePath) ?: error("Couldn't find resource")
        return Path.of(url.toURI())
    }
}

class TempFileResourceLoader(private val tempFile: File) : ResourceLoader {
    override fun ofDay(day: Int) = Path.of(tempFile.path)
}
