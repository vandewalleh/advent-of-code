@file:JvmName("Days")

package be.vandewalleh.aoc.utils.factory

import be.vandewalleh.aoc.utils.input.TempFileResourceLoader
import io.micronaut.context.BeanContext
import java.io.File

fun <T> createDay(beanType: Class<T>): T = BeanContext.run().getBean(beanType)

inline fun <reified T> createDay() = createDay(T::class.java)

// A custom resourceLoader that returns a string should be more appropriate but ¯\_(ツ)_/¯
private fun BeanContext.registerExampleLoader(example: String) {
    registerSingleton(TempFileResourceLoader(File.createTempFile("aoc-example", ".txt").apply {
        writeText(example)
    }))
}

fun <T> createDay(beanType: Class<T>, example: String): T {
    return BeanContext.build()
        .apply { registerExampleLoader(example) }
        .start()
        .getBean(beanType)
}

inline fun <reified T> createDay(example: String): T = createDay(T::class.java, example)
