package be.vandewalleh.aoc.utils.input

import be.vandewalleh.aoc.utils.factory.createDay
import com.google.common.jimfs.Jimfs
import io.micronaut.context.annotation.Replaces
import javax.inject.Singleton
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.writeText
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class DayTest {

    @Singleton
    @Replaces(ResourceLoaderImpl::class)
    @ExperimentalPathApi
    class FakeResourceLoader : ResourceLoader {
        private val inMemoryFs = Jimfs.newFileSystem().apply {
            getPath("1").writeText("blablabla")
            getPath("2").writeText("1,+2,3,4,-5")
            getPath("3")
                .writeText(
                    """
                    1779
                    1737
                    1537
                    1167
                    1804
                    1873
                    """.trimIndent()
                )
            getPath("4")
                .also { println(it) }
                .writeText(
                    """
                    a
                    bb
                    ccc
                    """.trimIndent()
                )
        }
        override fun ofDay(day: Int) = inMemoryFs.getPath(day.toString())
    }

    @Day(1)
    class TextStringExample(@Text val input: Input<String>)

    @Day(2)
    class CsvIntArrayExample(@Csv val input: Input<IntArray>)

    @Day(3)
    class IntLinesExample(@Lines val input: Input<IntArray>)

    @Day(4)
    class StringLinesExample(@Lines val input: Input<List<String>>)

    @Test
    fun `check @Text String`() {
        val day = createDay<TextStringExample>()
        assertThat(day.input.value).isEqualTo("blablabla")
    }

    @Test
    fun `check @Csv IntArray`() {
        val day = createDay<CsvIntArrayExample>()
        assertThat(day.input.value).containsExactly(1, +2, 3, 4, -5)
    }

    @Test
    fun `check @Lines IntArray`() {
        val day = createDay<IntLinesExample>()
        assertThat(day.input.value).containsExactly(1779, 1737, 1537, 1167, 1804, 1873)
    }

    @Test
    fun `check @Lines strings`() {
        val day = createDay<StringLinesExample>()
        assertThat(day.input.value).containsExactly("a", "bb", "ccc")
    }

}
