plugins {
    id("kotlin-convention")
    kotlin("kapt")
}

dependencies {
    implementation(Libs.Micronaut.core)
    api(Libs.Micronaut.inject)
    implementation(Libs.Micronaut.kotlin)
    kapt(Libs.Micronaut.processor)

    implementation(Libs.Slf4J.api)
    implementation(Libs.Slf4J.kotlin)
    runtimeOnly(Libs.Slf4J.simple)

    testImplementation(Libs.Tests.jimfs)
    kaptTest(Libs.Micronaut.processor)
}
