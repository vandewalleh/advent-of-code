package be.vandewalleh.aoc.days

import be.vandewalleh.aoc.utils.input.Day
import be.vandewalleh.aoc.utils.input.Input
import be.vandewalleh.aoc.utils.input.Lines

@Day(1)
class Day01(@Lines input: Input<IntArray>) {
    private val items = input.value

    fun part1(): Int? {
        items.forEach { a ->
            items.forEach { b ->
                if (a + b == 2020) return a * b
            }
        }
        return null
    }

    fun part2(): Int? {
        items.forEach { a ->
            items.forEach { b ->
                items.forEach { c ->
                    if (a + b + c == 2020) return a * b * c
                }
            }
        }
        return null
    }

}

