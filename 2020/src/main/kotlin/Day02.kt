package be.vandewalleh.aoc.days

import be.vandewalleh.aoc.utils.input.Day
import be.vandewalleh.aoc.utils.input.Input
import be.vandewalleh.aoc.utils.input.Lines

@Day(2)
class Day02(@Lines input: Input<List<String>>) {
    private data class PasswordEntry(val range: IntRange, val letter: Char, val password: String)

    private val regex = "^(\\d+)-(\\d+) ([a-z]): (.*)$".toRegex()

    private val passwords = input.value.map {
        val (_, min, max, letter, password) = regex.find(it)!!.groupValues
        PasswordEntry(min.toInt()..max.toInt(), letter[0], password)
    }

    fun part1() = passwords.count { it.password.count { char -> char == it.letter } in it.range }

    fun part2() = passwords.count { (range, letter, pwd) ->
        (pwd[range.first - 1] == letter) xor (pwd[range.last - 1] == letter)
    }
}
