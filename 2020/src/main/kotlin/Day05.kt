package be.vandewalleh.aoc.days

import be.vandewalleh.aoc.utils.input.Day
import be.vandewalleh.aoc.utils.input.Input
import be.vandewalleh.aoc.utils.input.Lines

@Day(5)
class Day05(@Lines val input: Input<List<String>>) {

    private val ids = input.value.map {
        it.replace("F", "0")
            .replace("B", "1")
            .replace("L", "0")
            .replace("R", "1")
            .toInt(2)
    }.sorted()

    fun part1() = ids.last()

    fun part2() = ids.windowed(size = 2, step = 1)
        .find { (a, b) -> b - a > 1 }!!
        .first() + 1
}
