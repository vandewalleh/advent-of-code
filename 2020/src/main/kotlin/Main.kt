package be.vandewalleh.aoc.days

import be.vandewalleh.aoc.utils.factory.createDay

fun main() = with(createDay<Day25>()) {
    println(part1())
}
