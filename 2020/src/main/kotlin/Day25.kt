package be.vandewalleh.aoc.days

import be.vandewalleh.aoc.utils.input.Day
import be.vandewalleh.aoc.utils.input.Input
import be.vandewalleh.aoc.utils.input.Lines

@Day(25)
class Day25(@Lines val input: Input<IntArray>) {
    private val doorPublicKey = input.value[0]
    private val cardPublicKey = input.value[1]

    private fun encryptionKey(loopSize: Int, publicKey: Int) = transformSubjectNumber(loopSize, publicKey)

    private fun transformSubjectNumber(loopSize: Int, subjectNumber: Int): Int {
        var number = 1L
        repeat(loopSize) {
            number *= subjectNumber
            number %= 20201227
        }
        return number.toInt()
    }

    private fun transformSubjectNumberStartingWith(currentNumber: Long, subjectNumber: Int): Long {
        var number = currentNumber
        number *= subjectNumber
        number %= 20201227
        return number
    }

    private fun findLoopSize(expectedPublicKey: Long): Int {
        var computedPublicKey = 1L
        var loopSize = 0
        do {
            computedPublicKey = transformSubjectNumberStartingWith(computedPublicKey, 7)
            loopSize++
        } while (computedPublicKey != expectedPublicKey)
        return loopSize
    }

    fun part1(): Int {
        val cardLoopSize = findLoopSize(cardPublicKey.toLong())
        return encryptionKey(cardLoopSize, doorPublicKey)
    }

}
