package be.vandewalleh.aoc.days

import be.vandewalleh.aoc.utils.input.Day
import be.vandewalleh.aoc.utils.input.Input
import be.vandewalleh.aoc.utils.input.Lines
import kotlin.math.abs

@Day(12)
class Day12(@Lines val input: Input<List<String>>) {

    fun part1(): Int {
        var x = 0
        var y = 0
        var facing = "E"

        val dirs = listOf("N", "E", "S", "W")

        input.value.forEach {
            val dir = it.take(1)
            val steps = it.drop(1).toInt()

            when (dir) {
                "L" -> repeat(steps / 90) {
                    val i = (dirs.indexOf(facing) + 4 - 1) % 4
                    facing = dirs[i]
                }
                "R" -> repeat(steps / 90) {
                    val i = (dirs.indexOf(facing) + 1) % 4
                    facing = dirs[i]
                }
            }

            when (if (dir == "F") facing else dir) {
                "N" -> y -= steps
                "S" -> y += steps
                "W" -> x -= steps
                "E" -> x += steps
            }

        }

        return abs(x) + abs(y)
    }

    fun part2(): Int {
        var x = 0
        var y = 0

        var waypointX = 10
        var waypointY = -1

        input.value.forEach {
            val dir = it.take(1)
            val steps = it.drop(1).toInt()

            when (dir) {
                "L" -> repeat(steps / 90) {
                    val oldY = waypointY
                    waypointY = -waypointX
                    waypointX = oldY
                }
                "R" -> repeat(steps / 90) {
                    val oldY = waypointY
                    waypointY = waypointX
                    waypointX = -oldY
                }
                "F" -> {
                    x += waypointX * steps
                    y += waypointY * steps
                }
                "N" -> waypointY -= steps
                "S" -> waypointY += steps
                "W" -> waypointX -= steps
                "E" -> waypointX += steps
            }
        }

        return abs(x) + abs(y)
    }
}
