package be.vandewalleh.aoc.days

import be.vandewalleh.aoc.utils.input.Csv
import be.vandewalleh.aoc.utils.input.Day
import be.vandewalleh.aoc.utils.input.Input
import kotlin.math.abs
import org.eclipse.collections.impl.factory.primitive.IntObjectMaps

@Day(15)
class Day15(@Csv val input: Input<IntArray>) {

    private fun run(until: Int): Int {
        val start = input.value

        val map = IntObjectMaps.mutable.empty<IntArray>()

        var last = -1

        fun say(round: Int, number: Int) {
            val lastValues = map.get(number)
            val values = lastValues
                ?.let { intArrayOf(round, it.maxOrNull()!!) }
                ?: intArrayOf(round)

            map.put(number, values)
            last = number
        }

        for (i in 1..until) {
            if (i <= start.size) {
                say(i, start[i - 1])
            } else {
                val lastValues = map.get(last)
                if (lastValues?.size ?: 0 < 2) say(i, 0)
                else say(i, abs(lastValues[0] - lastValues[1]))
            }
        }
        return last
    }

    fun part1() = run(until = 2020)

    fun part2() = run(until = 30000000)
}
