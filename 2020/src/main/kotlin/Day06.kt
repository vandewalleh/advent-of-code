package be.vandewalleh.aoc.days

import be.vandewalleh.aoc.utils.input.Day
import be.vandewalleh.aoc.utils.input.Input
import be.vandewalleh.aoc.utils.input.Text
import org.eclipse.collections.impl.factory.primitive.CharBags

@Day(6)
class Day06(@Text val input: Input<String>) {

    private val groups = input.value.split("\n\n")

    fun part1() = groups.sumBy { it.replace("\n", "").toCharArray().toSet().size }

    fun part2() = groups.sumBy {
        val group = it.split("\n")
        val bag = CharBags.mutable.empty()

        group.forEach { chars: String -> chars.forEach { bag.add(it) } }

        bag.selectByOccurrences { group.size == it }.toSet().size()
    }

}
