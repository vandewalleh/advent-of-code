package be.vandewalleh.aoc.days

import be.vandewalleh.aoc.utils.input.Day
import be.vandewalleh.aoc.utils.input.Input
import be.vandewalleh.aoc.utils.input.Lines
import org.eclipse.collections.api.list.primitive.MutableIntList
import org.eclipse.collections.impl.factory.primitive.IntLists
import org.eclipse.collections.impl.factory.primitive.IntLongMaps

@Day(10)
class Day10(@Lines val input: Input<IntArray>) {

    fun part1(): Int {
        val sorted = IntLists.mutable.of(0, *input.value).apply {
            sortThis()
            add(last + 3)
        }.toArray().toList()

        var ones = 0
        var threes = 0

        sorted.zipWithNext().forEach { (a, b) ->
            if (a + 1 == b) ones++
            else if (a + 3 == b) threes++
        }

        return ones * threes
    }

    fun part2(): Long {
        val sorted: MutableIntList = IntLists.mutable.of(*input.value).apply { sortThis() }

        val map = IntLongMaps.mutable.empty().apply {
            put(0, 1L)
        }

        sorted.forEach { i ->
            map.put(i, map.get(i - 1) + map.get(i - 2) + map.get(i - 3))
        }

        return map.get(sorted.last)
    }

}
