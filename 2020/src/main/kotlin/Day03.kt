package be.vandewalleh.aoc.days

import be.vandewalleh.aoc.utils.input.Day
import be.vandewalleh.aoc.utils.input.Input
import be.vandewalleh.aoc.utils.input.Lines

@Day(3)
class Day03(@Lines val input: Input<List<String>>) {
    private data class Slope(val x: Int, val y: Int)

    private fun findSlope(slope: Slope): Int {
        val grid = input.value
        var trees = 0
        var x = 0
        var y = 0

        val width = grid.first().length

        while (y < grid.size - 1) {
            x += slope.x
            y += slope.y

            if (grid[y][x % width] == '#') trees++
        }

        return trees
    }

    fun part1() = findSlope(Slope(x = 3, y = 1))

    fun part2(): Long = listOf(
        Slope(x = 1, y = 1),
        Slope(x = 3, y = 1),
        Slope(x = 5, y = 1),
        Slope(x = 7, y = 1),
        Slope(x = 1, y = 2),
    )
        .map { findSlope(it).toLong() }
        .reduce { acc, trees -> acc * trees }
}
