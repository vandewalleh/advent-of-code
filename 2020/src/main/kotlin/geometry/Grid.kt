@file:Suppress("MemberVisibilityCanBePrivate")

package be.vandewalleh.aoc.days.geometry

class Grid<T> {
    val data: ArrayList<ArrayList<T>> = ArrayList()

    constructor(data: Iterable<Iterable<T>>) {
        data.forEach {
            this.data.add(ArrayList(it.toList()))
        }
    }

    constructor(data: Array<Array<T>>) {
        data.forEach {
            this.data.add(ArrayList(it.toList()))
        }
    }

    val width get() = data[0].size
    val lastColumnIndex get() = data[0].size - 1

    val height get() = data.size
    val lastRowIndex get() = data.size - 1

    operator fun get(x: Int, y: Int): T? {
        if (y !in 0..lastRowIndex) return null
        val row = data[y]
        return if (x !in 0..lastColumnIndex) null
        else row[x]
    }

    operator fun set(x: Int, y: Int, value: T) {
        data[y][x] = value
    }

    fun row(y: Int): List<T> = data[y]
    fun firstRow() = row(0)
    fun lastRow() = row(height - 1)

    fun column(x: Int): List<T> = data.map { it[x] }
    fun firstColumn() = column(0)
    fun lastColumn() = column(width - 1)

    fun edges() = listOf(row(0), column(0), row(lastRowIndex), column(lastColumnIndex))

    override fun toString() = buildString {
        data.forEach { line ->
            append(line.joinToString(""))
            appendLine()
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as Grid<*>
        if (data != other.data) return false
        return true
    }

    override fun hashCode() = data.hashCode()

}
