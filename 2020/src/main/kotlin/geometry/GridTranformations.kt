package be.vandewalleh.aoc.days.geometry

import java.util.*

private fun <T> ArrayList<T>.reversed(): ArrayList<T> {
    val out = ArrayList<T>(this.size)
    asReversed().forEach { out.add(it) }
    return out
}

fun <T> Grid<T>.flipVertically(): Grid<T> = Grid(data.reversed())

fun <T> Grid<T>.flipHorizontally(): Grid<T> {
    val out = ArrayList<ArrayList<T>>(height)
    for (y in 0 until height) {
        out.add(data[y].reversed())
    }
    return Grid(out)
}

fun <T> Grid<T>.rotateRight(): Grid<T> {
    val out = ArrayList<ArrayList<T>>(width)
    for (x in 0 until width) {
        out.add(ArrayList<T>(height))
        for (y in 0 until height) {
            out[x].add(data[y][x])
        }
    }
    return Grid(out).flipHorizontally()
}

fun <T> Grid<T>.rotateLeft(): Grid<T> {
    val data = flipHorizontally().data
    val out = ArrayList<ArrayList<T>>(width)
    for (x in 0 until width) {
        out.add(ArrayList<T>(height))
        for (y in 0 until height) {
            out[x].add(data[y][x])
        }
    }
    return Grid(out).flipHorizontally()
}

fun <T> Grid<T>.transformations(): Sequence<Grid<T>> = sequence {
    yield(this@transformations)
    yield(flipHorizontally())
    yield(flipVertically())
    yield(rotateLeft())
    yield(rotateRight())
    yield(flipHorizontally().flipVertically())
    yield(flipVertically().rotateRight())
    yield(flipVertically().rotateLeft())
}
