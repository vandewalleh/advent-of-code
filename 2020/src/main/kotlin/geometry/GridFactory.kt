package be.vandewalleh.aoc.days.geometry

fun gridOf(lines: List<String>): Grid<Char> =
    Grid(lines.map { it.toCharArray().toList() })

fun gridOf(lines: String): Grid<Char> =
    gridOf(lines.lines())
