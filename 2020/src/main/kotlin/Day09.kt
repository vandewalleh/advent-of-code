package be.vandewalleh.aoc.days

import be.vandewalleh.aoc.utils.input.Day
import be.vandewalleh.aoc.utils.input.Input
import be.vandewalleh.aoc.utils.input.Lines

@Day(9)
class Day09(@Lines val input: Input<LongArray>) {

    private var part1Result = 0L

    fun part1(): Long? {
        val longs = input.value

        for (windowStart in 0 until longs.size - 26) {
            val last = longs[windowStart + 25]
            if (!isValid(windowStart, last)) {
                part1Result = last
                return last
            }
        }

        return null
    }

    private fun isValid(windowStart: Int, last: Long): Boolean {
        for (i in windowStart until windowStart + 25) {
            for (j in windowStart + 1 until windowStart + 25) {
                val f = input.value[i]
                val s = input.value[j]
                if (f + s == last && f != s) return true
            }
        }
        return false
    }

    fun part2(): Long {
        var size = 2
        while (true) {
            for (startIndex in input.value.indices) {
                val lastIndex = input.value.size - 1 - size
                if (startIndex + size > lastIndex) break
                val slice = input.value.sliceArray(startIndex..startIndex + size)
                if (slice.sum() == part1Result) return slice.minOrNull()!! + slice.maxOrNull()!!
            }
            size++
        }
    }

}
