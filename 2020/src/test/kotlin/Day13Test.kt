package be.vandewalleh.aoc.days

import be.vandewalleh.aoc.utils.input.Input
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

class Day13Test {

    @CsvSource(
        value = [
            "           7,13 | 77",
            "     17,x,13,19 | 3417",
            "     67,7,59,61 | 754018",
            "   67,x,7,59,61 | 779210",
            "   67,7,x,59,61 | 1261476",
            "1789,37,47,1889 | 1202161486",
        ],
        delimiter = '|'
    )
    @ParameterizedTest
    fun examples(buses: String, answer: Long) {
        val input = Input(listOf("", buses))
        assertThat(Day13(input).part2()).isEqualTo(answer)
    }

}
