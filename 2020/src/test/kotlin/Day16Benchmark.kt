package be.vandewalleh.aoc.days

import be.vandewalleh.aoc.utils.factory.createDay
import java.util.concurrent.TimeUnit
import org.openjdk.jmh.annotations.*
import org.openjdk.jmh.infra.Blackhole
import org.openjdk.jmh.runner.Runner
import org.openjdk.jmh.runner.options.Options
import org.openjdk.jmh.runner.options.OptionsBuilder

/*
Benchmark             Mode  Cnt  Score   Error  Units
Day16Benchmark.part2  avgt    5  0.697 ± 0.026  ms/op
*/

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
@Warmup(iterations = 3)
@Measurement(iterations = 5)
open class Day16Benchmark {

    private val day16 = createDay<Day16>()

    @Benchmark
    fun part2(blackhole: Blackhole) {
        blackhole.consume(day16.part2())
    }
}


fun main() {
    val opt: Options = OptionsBuilder()
        .include(Day16Benchmark::class.simpleName)
        .forks(1)
        .build()

    Runner(opt).run()
}
