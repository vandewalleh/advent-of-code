package be.vandewalleh.aoc.days

import be.vandewalleh.aoc.utils.input.Input
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

class Day18Test {

    @CsvSource(value = [
        "1 + 2 * 3 + 4 * 5 + 6 , 231",
        "1 + (2 * 3) + (4 * (5 + 6)) , 51",
        "2 * 3 + (4 * 5) , 46",
        "5 + (8 * 3 + 9 + 3 * 4 * 3) , 1445", // !!!
        "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4)) , 669060",
        "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2 , 23340"
    ])
    @ParameterizedTest
    fun examplesPart2(input: String, output: Long) {
        val day18 = Day18(Input(listOf(input)))
        assertThat(day18.part2()).isEqualTo(output)
    }
}
