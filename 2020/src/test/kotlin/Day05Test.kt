package be.vandewalleh.aoc.days

import be.vandewalleh.aoc.utils.factory.createDay
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class Day05Test {

    private val day05 = createDay<Day05>()

    @Test
    fun part1() {
    	assertThat(day05.part1()).isEqualTo(813)
    }

    @Test
    fun part2() {
        assertThat(day05.part2()).isEqualTo(612)
    }

}
