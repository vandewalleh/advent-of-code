package be.vandewalleh.aoc.days

import be.vandewalleh.aoc.utils.input.Input
import be.vandewalleh.aoc.utils.input.createDay
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class Day03Test {

    @Nested
    inner class Example {
        private val example = listOf(
            "..##.......",
            "#...#...#..",
            ".#....#..#.",
            "..#.#...#.#",
            ".#...##..#.",
            "..#.##.....",
            ".#.#.#....#",
            ".#........#",
            "#.##...#...",
            "#...##....#",
            ".#..#...#.#",
        ).let { Input(it) }

        private val day03 = Day03(example)

        @Test
        fun `part1 result`() {
            assertThat(day03.part1()).isEqualTo(7)
        }

        @Test
        fun `part2 result`() {
            assertThat(day03.part2()).isEqualTo(336)
        }
    }


    @Nested
    inner class RealInput {
        private val day03 = createDay<Day03>()

        @Test
        fun `part1 result`() {
            assertThat(day03.part1()).isEqualTo(294)
        }

        @Test
        fun `part2 result`() {
            assertThat(day03.part2()).isEqualTo(5774564250)
        }
    }

}
