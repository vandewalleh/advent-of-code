package be.vandewalleh.aoc.days

import be.vandewalleh.aoc.utils.factory.createDay
import java.util.concurrent.TimeUnit
import org.openjdk.jmh.annotations.*
import org.openjdk.jmh.infra.Blackhole
import org.openjdk.jmh.runner.Runner
import org.openjdk.jmh.runner.options.OptionsBuilder

/*
Benchmark             Mode  Cnt  Score   Error  Units
Day18Benchmark.part1  avgt    3  0.253 ± 0.098  ms/op
Day18Benchmark.part2  avgt    3  0.283 ± 0.027  ms/op
*/

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
open class Day18Benchmark {

    private val day18 = createDay<Day18>()

    @Benchmark
    fun part1(blackhole: Blackhole) {
        blackhole.consume(day18.part1())
    }

    @Benchmark
    fun part2(blackhole: Blackhole) {
        blackhole.consume(day18.part2())
    }

}

fun main() {
    val opt = OptionsBuilder()
        .include(Day18Benchmark::class.simpleName)
        .forks(1)
        .build()

    Runner(opt).run()
}
