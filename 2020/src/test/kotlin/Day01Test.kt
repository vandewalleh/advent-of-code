package be.vandewalleh.aoc.days

import io.micronaut.context.BeanContext
import io.micronaut.context.getBean
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class Day01Test {

    private val day = BeanContext.run().getBean<Day01>()

    @Test
    fun `part1 result`() {
        assertThat(day.part1()).isEqualTo(32064)
    }

    @Test
    fun `part2 result`() {
        assertThat(day.part2()).isEqualTo(193598720)
    }

}
