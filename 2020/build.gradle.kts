plugins {
    id("kotlin-convention")
    kotlin("kapt")
}

dependencies {
    implementation(project(":utils"))

    kapt(Libs.Micronaut.processor)

    implementation(Libs.Slf4J.api)
    runtimeOnly(Libs.Slf4J.simple)

    implementation(Libs.eclipseCollections)

    testImplementation(Libs.Jmh.core)
    kaptTest(Libs.Jmh.processor)
}
